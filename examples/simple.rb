require_relative File.expand_path('../lib/jongleur.rb', __dir__)
require_relative './example_tasks' 
include Jongleur

test_graph = {
      A: [:C],
      B: [:D],
      C: [:E],
      D: [:E],
      E: []
    }

API.add_task_graph test_graph

API.run do |on|
    puts ">>>> client does something"
    on.completed do |task_matrix|
      puts "Jongleur run is complete \n"
       puts API.successful_tasks(task_matrix)
       puts "oh-oh" if API.failed_tasks(task_matrix).length > 0 
       # print tasks in order of finishing time
       task_matrix.sort_by { |x| x.finish_time }.map { |t| STDOUT.puts t.name }
    end
    puts ">>>> client does another thing"
  end
