# frozen_string_literal: true
require 'graphviz'
require 'json'
require 'set'
require 'hollerback'
require_relative 'jongleur/version'
require_relative 'jongleur/helpers'
require_relative 'jongleur/worker_task'
require_relative 'jongleur/implementation'
require_relative 'jongleur/api'
require 'logger'

# this is the gem's main module
module Jongleur
  # a Task is a representation of the status of an executable Jongleur class,
  # i.e. a class derived from WorkerTask and the process that's executing that class
  # @see https://ruby-doc.org/core-2.4.3/Process/Status.html
  #
  # @!attribute name
  #   @return [String] the class (WorkerTask) name that's executing this process
  # @!attribute pid
  #   @return [Integer] the process id accoding to the OS
  # @!attribute running
  #   @return [Boolean] true if the process is running
  # @!attribute exit_status
  #   @return [Integer, Nil] the process's return code when the process is exited
  #   Usually 0 for success, 1 for error or Nil otherwise
  # @!attribute finish_time
  #   @return [Float, 0] the timestamp of process completion as a floating point number 
  #   of seconds since the Epoch 
  # @!attribute success_status
  #   @return [Boolean, Nil] true if process finished successfully, false if it didn't 
  #       or nil if process didn't exit properly.
  Task = Struct.new(:name, :pid, :running, :exit_status, :finish_time, :success_status)

  $stdout.sync = true

  module StatusCodes
    PROCESS_NOT_YET_RAN = -1
    TASK_NOT_IN_TASK_MATRIX = -8
    TASK_NOT_IN_TASK_GRAPH = -9
    SUCCESS_STATUS_UNDETERMINED = -2
  end

end # module

