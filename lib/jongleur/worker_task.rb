# frozen_string_literal: true

module Jongleur
  # This is a Base class for all task classes executed by Jongleur.
  # Every class declared and used in Jongleur must inherit from <WorkerTask>
  class WorkerTask
    def initialize(**other_args)
      other_args.each do |key, val|
        var_name = "@#{key}"
        instance_variable_set(var_name, val)
        self.class.send(:attr_accessor, key.to_s)
      end
    end
  end

  # returns the task description
  class << self
    attr_reader :desc
  end
end # class
