# frozen_string_literal: true

# this module contains generic helper methods that are used
# for implementation purposes
module Helper
  def contains_array?(an_array)
    (self & an_array).size == an_array.size
  end
end
