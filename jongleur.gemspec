# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jongleur/version'

Gem::Specification.new do |spec|
  spec.name          = 'jongleur'
  spec.version       = Jongleur::VERSION
  spec.authors       = ['Fred Heath']
  spec.email         = ['fred@bootstrap.me.uk']

  spec.summary       = 'A multi-processing task scheduler and manager.'
  spec.description   = 'Launches, schedules and manages tasks represented in a DAG, as multiple processes'
  spec.homepage      = 'https://gitlab.com/RedFred7/Jongleur'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'graphviz', '~> 1.1'
  spec.add_dependency 'os', '~> 1.0'
  spec.add_dependency 'hollerback', '~> 0.1'

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'pry-byebug', '~> 3.4'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.58'
  spec.add_development_dependency 'simplecov', '~> 0.9'
  spec.add_development_dependency 'yard', '~> 0.9'

  spec.post_install_message = 'Before using Jongleur, please make sure your system has the Graphviz package installed!'
end
