# frozen_string_literal: true
require 'simplecov'
SimpleCov.add_filter ['spec']
SimpleCov.start
require 'bundler/setup'
require 'uri'
$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'jongleur'
require_relative 'support/test_tasks'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    $stdout = StringIO.new # capture output from test runs
  end

  config.before(:each) do
    $stdout.truncate(0)
    load File.expand_path('support/test_tasks.rb', File.dirname(__FILE__)) # reset test graph
  end

  config.after(:suite) do
    # must reset stdout, or other code that needs it (like SimpleCov) won't be able to use it
    STDOUT.puts '>>>>>>>>>>>>>> End of test suite'
    $stdout = STDOUT
  end

end
