# frozen_string_literal: true
include Jongleur

RSpec.describe Jongleur do

  let(:test_graph) do
    {
      A: %i[B C],
      B: [:D],
      C: [:D],
      D: [:E],
      E: []
    }
  end

  context "during execution" do
    it "detects tasks that have no implementation" do
      test_graph = {
        A: [:C],
        B: [:C],
        C: [:G],
        G: [:E],
        E: []
      }
      API.add_task_graph test_graph
      expect do
        API.run { |on| on.completed { |task_matrix| } }
      end.to raise_error(RuntimeError, /Not all the tasks in the Task Graph are implemented/)
    end
  end

  context "before execution" do
    it "checks Task Graph is initialised" do
      test_graph = [
        [:A, [:C]],
        [:B, [:C]],
        [:C, [:G]],
        [:G, [:E]],
        [:E, []]
      ]
      expect { API.add_task_graph test_graph }.to raise_error(ArgumentError, /Value should be Hash/)
    end

    it "checks dependent tasks not in a list" do
      test_graph = {
        A: [:C],
        B: :C, # Failure point
        C: [:G],
        G: [:E],
        E: []
      }
      expect { API.add_task_graph test_graph }.to raise_error(ArgumentError, /Dependent Tasks should be wrapped in an Array/)
    end
  end

  context "after execution" do
    it "shows user which tasks have failed and which haven't" do
      C.class_eval { define_method('execute') { puts 'my C....'; exit 34 } }
      API.add_task_graph test_graph
      begin
        API.run do |on|
          successful_tasks, failed_tasks, unstarted_tasks, hung_tasks = Array.new, Array.new, Array.new, Array.new
          on.completed do |task_matrix|
            successful_tasks = API.successful_tasks(task_matrix).reduce([]) {|arr, x| arr << x.name}
            failed_tasks = API.failed_tasks(task_matrix).reduce([]) {|arr, x| arr << x.name}
            unstarted_tasks = API.not_ran_tasks(task_matrix).reduce([]) {|arr, x| arr << x.name}
            hung_tasks = API.hung_tasks(task_matrix).reduce([]) {|arr, x| arr << x.name}
            expect(successful_tasks.sort).to eq [:A, :B]
            expect(failed_tasks.sort).to eq [:C]
            expect(unstarted_tasks.sort).to eq [:D, :E]
            expect(hung_tasks.sort).to eq []
          end
        end
      rescue SystemExit
      end
    end
  end

end
