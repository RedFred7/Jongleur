# frozen_string_literal: true

# rubocop:disable Style/MixinUsage

include Jongleur

RSpec.describe Jongleur do
  let(:test_graph) do
    { A: %i[B C],
      B: [:D],
      C: [:D],
      D: [:E],
      E: [] }
  end

  it 'tests #get_process_id' do
    test_graph = {
      A: [:C]
    }
    API.add_task_graph test_graph
    expect(Implementation.get_process_id(:A)).to eq StatusCodes::PROCESS_NOT_YET_RAN
    expect(Implementation.get_process_id(:X)).to eq StatusCodes::TASK_NOT_IN_TASK_GRAPH
  end

  it 'tests #get_task_matrix' do
    test_graph = {
      A: [:C],
      B: [:D],
      C: [:E],
      D: [:E],
      E: []
    }

    task_matrix = Implementation.build_task_matrix test_graph
    expect(task_matrix).to be_an_instance_of(Array)
    expect(task_matrix.size).to eq 5
    task_matrix.each do |task|
      expect(task.pid).to eq StatusCodes::PROCESS_NOT_YET_RAN
      expect(task.running).to be false
      expect(task.exit_status).to be_nil
      expect(task.success_status).to be_nil
    end

    task_matrix = Implementation.build_task_matrix test_graph.clear
    expect(task_matrix).to eq []
  end

  it 'tests #get_predecessors' do
    API.add_task_graph test_graph
    expect(Implementation.get_predecessors(:A)).to eq []
    expect(Implementation.get_predecessors(:B)).to eq [:A]
    expect(Implementation.get_predecessors(:C)).to eq [:A]
    expect(Implementation.get_predecessors(:D)).to eq %i[B C]
    expect(Implementation.get_predecessors(:E)).to eq [:D]
  end

  it 'tests #valid_tasks?' do
    expect(Implementation.valid_tasks?([:X])).to be false
    expect(Implementation.valid_tasks?([:A])).to be true
    expect(Implementation.valid_tasks?([:E])).to be true
    expect(Implementation.valid_tasks?(%i[A B C])).to be true
    expect(Implementation.valid_tasks?(%i[A B X])).to be false
  end

  it 'tests #get_exit_status' do
    API.add_task_graph test_graph
    C.class_eval { define_method('execute') { puts 'my C task'; sleep 1; exit 99 } }
    begin
      API.run do |on|
        on.completed do |task_matrix|
          expect(Implementation.get_exit_status(:A)).to eq 0
          expect(Implementation.get_exit_status(:B)).to eq 0
          expect(Implementation.get_exit_status(:C)).to eq 99
          expect(Implementation.get_exit_status(:D)).to be_nil
          expect(Implementation.get_exit_status(:E)).to be_nil
          expect(Implementation.get_exit_status(:X)).to eq StatusCodes::TASK_NOT_IN_TASK_MATRIX
        end
      end
    rescue SystemExit
    end
  end

  it 'tests #task_failed?' do
    API.add_task_graph test_graph
    C.class_eval { define_method('execute') { puts 'my C task'; sleep 1; exit 99 } }
    begin
      API.run do |on|
        on.completed do |task_matrix|
          expect(Implementation.task_failed?(:A)).to be false
          expect(Implementation.task_failed?(:B)).to be false
          expect(Implementation.task_failed?(:C)).to be true
          expect(Implementation.task_failed?(:D)).to eq false
          expect(Implementation.task_failed?(:E)).to eq false
          expect(Implementation.task_failed?(:X)).to eq StatusCodes::TASK_NOT_IN_TASK_MATRIX
        end
      end
    rescue SystemExit
    end
  end

  it 'tests #tasks_without_predecessors' do
    API.add_task_graph test_graph
    expect(Implementation.tasks_without_predecessors.size).to eq 1
    expect(Implementation.tasks_without_predecessors.first.name).to eq :A
  end

  it 'tests #find_task_by' do
    API.add_task_graph test_graph
    C.class_eval { define_method('execute') { puts 'my C task'; sleep 1; exit 99 } }
    begin
      API.run do |on|
        on.completed do |task_matrix|
          expect(Implementation.find_task_by(:name, :B).exit_status).to eq 0
          expect(Implementation.find_task_by(:name, :C).exit_status).to eq 99
          expect(Implementation.find_task_by(:exit_status, 99).name).to eq :C
          expect(Implementation.find_task_by(:success_status, 'not there')).to be_nil
          expect { |b| 5.tap(&b) }.to yield_with_args(5)
          expect { |b| Implementation.find_task_by(:name, :C, &b) }.to yield_with_args(Jongleur::Task)
        end
      end
    rescue SystemExit
    end
  end
end

# rubocop:enable Style/MixinUsage
