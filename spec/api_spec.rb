# frozen_string_literal: true
include Jongleur

RSpec.describe Jongleur do
  require 'tmpdir'

  let(:test_graph) do
    {
      A: %i[B C],
      B: [:D],
      C: [:D],
      D: [:E],
      E: []
    }
  end

  context "all tasks are successful" do
    it 'runs DAG 1' do
      test_graph = {
        A: [:C],
        B: [:D],
        C: [:E],
        D: [:E],
        E: []
      }

      API.add_task_graph test_graph
      begin
        API.run do |on|
          on.completed do |task_matrix|
            expect(task_matrix.sort_by { |x| x.finish_time }.map { |t| t.name }).to eq %i[A B C D E]
          end
        end
      rescue SystemExit
      end
    end

    it 'runs DAG 2' do
      C.class_eval { define_method('execute') { puts 'my C....'; sleep 4 } }
      API.add_task_graph test_graph
      begin
        API.run do |on|
          on.completed do |task_matrix|
            expect(task_matrix.sort_by { |x| x.finish_time }.map { |t| t.name }).to eq %i[A B C D E]
          end
        end
      rescue SystemExit
      end
    end

    it 'runs DAG 3' do
      B.class_eval { define_method('execute') { puts 'my B....'; sleep 4 } }
      API.add_task_graph test_graph
      begin
        API.run do |on|
          on.completed do |task_matrix|
            expect(task_matrix.sort_by { |x| x.finish_time }.map { |t| t.name }).to eq %i[A C B D E]
          end
        end
      rescue SystemExit
      end
    end

    it "runs DAG 4" do
      test_graph = {
        A: %i[B C D],
        C: [:D],
        B: [],
        D: []
      }
      API.add_task_graph test_graph
      begin
        API.run do |on|
          on.completed do |task_matrix|
            expect(task_matrix.sort_by { |x| x.finish_time }.map { |t| t.name }).to eq %i[A C B D]
          end
        end
      rescue SystemExit
      end
    end

    it 'runs parallel tasks' do
      test_graph = {
        A: %i[B C D E F],
        B: [],
        C: [],
        D: [],
        E: [],
        F: []
      }

      B.class_eval { define_method('execute') { puts 'my B....'; sleep 1 } }
      C.class_eval { define_method('execute') { puts 'my C....'; sleep 2 } }
      D.class_eval { define_method('execute') { puts 'my D....'; sleep 3 } }
      E.class_eval { define_method('execute') { puts 'my E....'; sleep 4 } }
      F.class_eval { define_method('execute') { puts 'my F....'; sleep 5 } }

      API.add_task_graph test_graph
      begin
        API.run do |on|
          on.completed do |task_matrix|
            expect(task_matrix.sort_by { |x| x.finish_time }.map { |t| t.name }).to eq %i[A B C D E F]
          end
        end
      rescue SystemExit
      end
    end

    it 'runs sequential tasks' do
      test_graph = {
        F: [:E],
        E: [:D],
        D: [:C],
        C: [:B],
        B: [:A]
      }

      B.class_eval { define_method('execute') { puts 'my B....'; sleep 1 } }
      C.class_eval { define_method('execute') { puts 'my C....'; sleep 2 } }
      D.class_eval { define_method('execute') { puts 'my D....'; sleep 3 } }
      E.class_eval { define_method('execute') { puts 'my E....'; sleep 4 } }
      F.class_eval { define_method('execute') { puts 'my F....'; sleep 5 } }

      API.add_task_graph test_graph
      begin
        API.run do |on|
          on.completed do |task_matrix|
            expect(task_matrix.sort_by { |x| x.finish_time }.map { |t| t.name }).to eq %i[F E D C B A]
          end
        end
      rescue SystemExit
      end
    end

    it 'prints DAG to PDF' do
      API.add_task_graph test_graph
      temp_dir = Dir.mktmpdir
      pdf_file = API.print_graph(temp_dir)
      expect(File.exist?(pdf_file)).to be true
      FileUtils.remove_entry temp_dir
    end

    it "gets predecessor pids" do
      API.add_task_graph test_graph
      begin
        API.run do |on|
          on.completed do |task_matrix|
            expect(API.get_predecessor_pids(:A).size).to eq 0
            expect(API.get_predecessor_pids(:B).size).to eq 1
            expect(API.get_predecessor_pids(:C).size).to eq 1
            expect(API.get_predecessor_pids(:D).size).to eq 2
            expect(API.get_predecessor_pids(:E).size).to eq 1
            expect(API.get_predecessor_pids(:B).flatten.include?(StatusCodes::PROCESS_NOT_YET_RAN)).to be false
            expect(API.get_predecessor_pids(:C).flatten.include?(StatusCodes::PROCESS_NOT_YET_RAN)).to be false
            expect(API.get_predecessor_pids(:D).flatten.include?(StatusCodes::PROCESS_NOT_YET_RAN)).to be false
            expect(API.get_predecessor_pids(:E).flatten.include?(StatusCodes::PROCESS_NOT_YET_RAN)).to be false
          end
        end
      rescue SystemExit
      end
    end
  end
end
