# Change Log
All notable changes to this project will be documented in this file.


## [1.0.0] - 27-Aug-2018
### Initial Release.

## [1.0.1] - 27-Aug-2018
### Release management issues.

## [1.0.2] - 27-Aug-2018
### Release management issues.

## [1.0.3] - 27-Aug-2018
### Release management issues.

## [1.0.4] - 20-Oct-2018
### Added
- bin/setup enhanced to install Graphviz dependency

## [1.1.0] - 31-Dec-2018
### Added
- 'Completed' asynchronous callback when execution is finished (via Hollerback gem)
- Added finish_time atrribute to task_matrix as to timestamp process completion

### Changed
- Re-written specs to use async callback

## [1.1.1] - 05-Jan-2019

### Changed
- Moved invocation of task executions out of Signal Trap context
